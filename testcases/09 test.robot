*** Settings ***
Library    OperatingSystem
Library  RequestsLibrary
Library    Collections

*** Test Cases ***
TC0901 test get data from website
    Create Session      wiki       https://api.jsonbin.io
    ${response}=    Get Request     wiki    /b/5c388d0c7b31f426f8567057
    ${json}=    Evaluate    json.loads($response.text)    modules=json
    ${sumall}=  Set Variable    0
    FOR    ${i}    IN    @{json}
          ${friends}=     Set Variable      ${i}[friends]
          ${sumbalance}=       Calculate balance ${friends}
          ${sumall}=   Evaluate    ${sumall}+${sumbalance}
    END
    Log     summary all friends balance = ${sumall}

TC0902 test dummy api
    #1. check if name exists -> delete it
    #2. add new employee
    #3. check if new employee added
    #4. update salary
    #5. check if salary updated
    #6. delete employee
    #7. check if employee deleted

    [Tags]  employee
    create session  dummy    https://54.169.72.70:8000
    ${jsonData}=  get employee members
    ${dataSet}=    create dictionary  name=DummyMaria    salary=30000    age=1000
    ${my_em_name}=  set variable  ${dataSet}[name]
    check employee name avariable ${jsonData} ${my_em_name}
    ${dataResponse}=  send create employee ${dataSet}
    ${dataResponse}=  to json  ${dataResponse}
    ${my_employee_ID}=  set variable  ${dataResponse}[id]
    ${check_create}=  get employee Id from ${my_employee_ID}
    check direction of json ${dataSet} ${check_create}
    ${my_em_update}=  create dictionary  name=${my_em_name}    salary=120000    age=25
    send update employee ${my_employee_ID} ${my_em_update}
    ${my_em_jsonData}=  get employee Id from ${my_employee_ID}
    ${my_em_name}=  set variable  ${my_em_jsonData}[employee_name]
    ${my_update_em_name}=  set variable  ${my_em_update}[name]
    check direction of json ${my_em_update} ${my_em_jsonData}
    send delete employee ${my_employee_ID}
    ${my_em_jsonData}=  get employee Id from ${my_employee_ID}
    should be equal as strings  ${my_em_jsonData}  False

*** Keywords ***
Calculate balance ${friends}
    ${sumbalance}=      Set Variable    0
    FOR    ${j}    IN    @{friends}
          ${sumbalance}=     Evaluate     ${sumbalance}+${j}[balance]
    END
    Return From Keyword     ${sumbalance}

Check employees ${name} is exists  
    ${response}=    Get Request     dummy      /api/v1/employees
    ${json}=    Evaluate    json.loads($response.text)    modules=json
    FOR    ${i}    IN    @{json}
        Run Keyword If    $i["employee_name"]=="${name}"       Return From Keyword     0
        ...     ELSE    Continue For Loop         
    END
    Return From Keyword     1

get employee members
    ${response}=  get request  dummy    /api/v1/employees
    ${jsonData}=  to json  ${response.content}
    return from keyword  ${jsonData}

get employee Id from ${em_name}
    ${response}=  get request  dummy    /api/v1/employee/${em_name}
    ${jsonData}=  to json  ${response.content}
    return from keyword  ${jsonData}

send delete employee ${em_id}
    ${response}=  delete request  dummy    /api/v1/delete/${em_id}

send create employee ${data}
    ${header}=  create dictionary  Content-Type=application/json  User-agent=${EMPTY}
    ${response}=  post request  dummy    /api/v1/create    headers=${header}    data=${data}
    return from keyword  ${response.text}

send update employee ${my_em_id} ${data}
    ${header}=  create dictionary  Content-Type=application/json  User-agent=${EMPTY}
    ${response}=  put request  dummy    /api/v1/update/${my_em_id}    headers=${header}    data=${data}

check employee name avariable ${data} ${em_name}
    FOR  ${item}  IN  @{data}
        log  ${item}
        ${key}=  set variable    ${item}[employee_name]
        run keyword if  $em_name == $key    run keywords  send delete employee ${item}[id]
        ...  AND      exit for loop
    END

check direction of json ${myData} ${thisData}
    should be equal as strings    ${myData}[name]    ${thisData}[employee_name]
    should be equal as strings    ${myData}[salary]    ${thisData}[employee_salary]
    should be equal as strings    ${myData}[age]    ${thisData}[employee_age]


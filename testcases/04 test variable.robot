*** Settings ***
Library     Collections
Variables   ../resources/data.yaml

*** Variables ***
${x}    10
${y}    ${10}
${z}    ${10.0}
${list2}      10      ${10}       ${10.0}     20      30


*** Test Cases ***
TC0401 test scalar variable
    ${xx}=      Set Variable    10
    ${yy}=      Set Variable    ${10}
    ${zz}=      Set Variable    ${10.0}

TC0402 test list variable
    ${list1}=   Create List     10      ${10}       ${10.0}     20      30
    Log     ${list1}
    Log List       ${list1}

    Log     ${list1}[3]
    Log     ${list1[3]}
    Log     ${list1}[3:5]
    Log     ${list1}[3:]
    Log     ${list1}[3:-1]

    Append To List      ${list1}    60      70
    Remove From List      ${list1}    3
    Log     ${list1}

    # loop item in list by length
    ${len}=     Get Length      ${list1}
    FOR     ${i}    IN RANGE    0   ${len}
        Log     index=${i}=${list1}[${i}]
    END

    # loop item in list by iteration beware!!!! @ simbol if loop in list
    FOR     ${i}    IN      @{list1}
        Log     value=${i}
    END

TC0403 test dictionary
    ${dic}=     Create dictionary   a=10    b=20    c=30
    Log Dictionary      ${dic}
    Log     ${dic}[b]
    Log     ${dic["b"]}
    Set To Dictionary   ${dic}      d=40    e=50
    Remove From Dictionary      ${dic}  a

    FOR     ${k}    IN      @{dic}
        Log     key=${k}, value=${dic}[${k}]
    END

    FOR     ${k}    ${v}     IN ZIP     ${dic.keys()}   ${dic.values()}
        Log      key=${k},value=${v}
    END

TC0404 test data from yaml
    Log     ${string}


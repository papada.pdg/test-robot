*** Test Cases ***
TC0301 test if condition
    #Run Keyword If      condition   keyword    *args
    ${x}    Set Variable    10
    Run Keyword If      ${x}>10   do something when if met
    ...     ElSE IF     ${x}==10    Log     x is equal 10  
    ...     ELSE    Run Keywords    Log     else do 11111
    ...     AND     Log     else do 222

TC0302 test if condition for string
    ${msg}=     Set Variable    hello
    #$msg -> send variable and type, ${msg} -> send value
    Run Keyword If      $msg=="hello"   log     result correct
    ...     ELSE    result incorrect


TC0303 test for loop
    ${sum}=      Set Variable    0
    #robot < 3.1
    :FOR    ${i}    IN RANGE    0   10
    \      ${sum}=     Evaluate    ${sum}+${i}
    Log     sum=${sum}
    
    #robot > 3.1
    FOR    ${i}    IN RANGE    0   10
          ${sum}=     Evaluate    ${sum}+${i}
    END
    Log     sum=${sum}

TC0304 test nested loop
    ${sumall}=      Set Variable    0
    FOR    ${i}    IN RANGE    0   10
        ${sumall}=     do nested loop for ${i}
        ${sum}=     Evaluate    ${sumall}+${i}
    END
    Log     sumall=${sumall}

TC0305 test continue and exit
    FOR    ${i}    IN RANGE    0   10
        Continue For Loop If    ${i}%2==1
        Exit For Loop If    ${i}>=8
        Log     i=${i}
    END

*** Keywords ***
do something when if met
    Log     1111
    Log     2222

do nested loop for ${i}
    ${sum}=     Set Variable    0
    FOR    ${j}    IN RANGE    0   10
        ${sum}=     Evaluate    ${sum}+(${i}*${j})
    END
    Return From Keyword     ${sum}
*** Settings ***
Resource    ../keywords/calculator keyword.robot

*** Test Cases ***
TC0201 test add operation
    #1 prepare data
    ${x}=       Set Variable        3
    ${y}=       Set Variable        4
    ${result}=      Set Variable        ${7}
    #${number} -> Integer
    #2 do test 
    ${actual result}=       Evaluate    ${x}+${y}
    #3 validate result
    Should be Equal As Integers     ${actual result}    ${result}

TC0202 test subtract operation
    #1 prepare data
    ${x}=       Set Variable        3
    ${y}=       Set Variable        4
    ${result}=      Set Variable        -1
    ${actual result}=   do substract operation  ${x}  ${y}
    Should be Equal As Integers     ${actual result}    ${result}

TC0203 test multiply operation data driven
    [Template]      test multiply operation
    #x     y    result
    3     4     12
    5     8    40

TC0204 test divide operation gherkins style
    Given x is 12 and y is 4
    When click divide button
    Then monitor should show 3

*** Keywords ***
x is ${x} and y is ${y}
    Log     x=${x} y=${y}
    Set Test Variable   ${x}
    Set Test Variable   ${y}

click divide button
    ${actual result}=       Evaluate    ${x}/${y}
    Set Test Variable   ${actual result}

monitor should show ${expected result}
    Should be Equal As Integers     ${actual result}    ${expected result}

test multiply operation
    [Arguments]     ${x}    ${y}    ${result}
    #1 prepare data
    #2 do test 
    ${actual_result}=       Evaluate    ${x}*${y}
    #3 validate result
    Should be Equal As Integers     ${actual result}    ${result}
*** Variables ***
${Mark}     Mark
${james}    James

*** Test Cases ***
TC0101 test hello world
    Log     hello world
    Log To Console      hello world on console

TC0102 test say hello to
    say hello to  John

TC0103 test say hello to friends
    say hello to friends  John  Devid  Marry
    say hello to friends    John    Devid
    say hello to friends  friend1=John  friend2=David

TC0104 test get my friend
    ${friend1}      ${friend2}=     get my friend
    Log     hello my friend ${friend1} and ${friend2}

*** Keywords ***
say hello to
    [Arguments]     ${name}
    Log     hello my friend ${name}

say hello to friends
    [Arguments]     ${friend1}      ${friend2}      ${friend3}=Marry
    Log     Hello my friend ${friend1}, ${friend2} and ${friend3}

get my friend
    ${friend1}=     Set Variable    Thomas
    ${friend2}=     Set Variable    ${Mark}
    Return From Keyword     ${friend1}  ${friend2}
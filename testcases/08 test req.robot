*** Settings ***
Library     RequestsLibrary

*** Test Cases ***
TC0801 test get data from wiki
    Create Session      wiki       https://en.wikipedia.org
    ${response}=    Get Request     wiki    /wiki/Main_Page
    Log     ${response.text}
    Log     ${response.headers}
    Log     ${response.status_code}
    #Log     ${response.json()}


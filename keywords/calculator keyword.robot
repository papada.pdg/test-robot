*** keywords ***
do substract operation
    [Arguments]     ${x}    ${y}
    ${actual_result}=       Evaluate    ${x}-${y}
    Return From keyword    ${actual_result}
# Read Me
Test Robot Framework
this is made by Robot Framework, you need to install robot framework and python first

## Project setup
1. mac/os install via homebrew
```
brew update
brew install pyenv
```

2. check if file ~/.bash_project is existing then push content below in and change <username> to be your
```
export PYENVROOT=/Users/<username>/.pyenv
export PATH=$PYENVROOT/shims:$PYENVROOT/bin:$PATH
```

3. install python
```
pyenv install 3.7.4
pyenv global 3.7.4
```

4. install framework and libraries
```
pip install robotframework
pip install robotframework-requests
pip install PyYAML
```

**Run project**
> Robot -d report -L TRACE *<name_file>*